# hsDockerfiles

A set of dockerfiles for using Apache Hadoop and Spark either alone or together
with Zeppelin or Jupyter notebooks. See individual folders for more information.

## Build

The images can be built using the included `build.sh` script whcih will build
the contained images with tags `danlilja/hsbase`, `danlilja/hszeppelin`,
`danlilja/hsjupyter` and `danlilja/hskafka`. Alternatively you can find build
instructions for each separate docker image in their respective folders.

## Docker hub

The images are also available at Docker hub.
