# hsBase

A docker image containing Apache Hadoop and Apache Spark. It is preconfigured
for using hdfs and Yarn in pseudo-distributed mode, i.e. launching a master and
a worker on the same node.

## Build

To build the image run the command `docker build -t <tag> .` in the `hsBase`
folder of the project. Replace `<tag>` with a tag of your choosing to easily
reference the image later for builds. The suggested tag is `danlilja/hsbase`,
which will be used for the remainder of this document.

If you want anythong other than the standard Apache Hadoop configuration you can
add those configuration files to the `configs/haddop` folder and those files
will automatically be copied to the Hadoop configuration folder during the
build.

## Usage

To start a container use the command `docker run -it danlilja/hsbase`. It will
automatically start an hdfs namenode and an hdfs datanode, a Yarn
resourcemanager and a nodemanager and then wait. If you want to override this
behavior use, for example, `docker run -it --entrypoint bash danlilja/hsbase` to
instead start `bash` in the container.

The image also supports four command line options. These are:

* `--format`: Formats the hdfs namenode before starting hdfs and yarn.
* `--worker`: Stops the container from starting hdfs or yarn. Is intended to be
  used together with the `--hdfs` or `--yarn` options.
* `--hdfs`: Starts hdfs namenode and datanode instead of both hdfs and yarn. If
  used with `--worker` it only starts a datanode to connect to an already
  running namenode.
* `--yarn`: Starts yarn resourcemanager and nodemanager instead of both hdfs and
  yarn. If used with `--worker` it only starts a nodemanager to connect to an
  already running resourcemanager.

**Note**: Unless you mount an existing hdfs file system to the container you
need to format the namenode for hdfs to run.

## Ports

The following ports are exposed by the dockerfile:

- hdfs webUI: 50070
- Yarn webUI: 8088
- Spark master webUI: 7070
- Spark worker webUI: 8081

**Note**: The Spark master webUI port is not the default 8080 to avoid collision
with Zeppelin which also uses port 8080 by default.
