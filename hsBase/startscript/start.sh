#! /bin/bash

hdfs=0
yarn=0
worker=0
format=0

/etc/init.d/ssh start

while [[ $1 != "" ]]; do
    case $1 in
        --hdfs )
            hdfs=1
            ;;
        --yarn )
            yarn=1
            ;;
        --worker )
            worker=1
            ;;
        --format )
            format=1
            ;;
    esac
    shift
done

if [[ $format = 1 ]]; then
    hdfs namenode -format
fi

if [[ $worker = 1 ]]; then
    if [[ $hdfs = 1 ]]; then
        sleep 20
        hdfs datanode &
    fi
    if [[ $yarn = 1 ]]; then
        sleep 20
        yarn nodemanager &
    fi
else
    if [[ $hdfs = 1 ]]; then
        hdfs namenode &
        hdfs datanode &
    fi
    if [[ $yarn = 1 ]]; then
        yarn resourcemanager &
        yarn nodemanager &
    fi
    if [[ $hdfs = 0 && $yarn = 0 ]]; then
        hdfs namenode &
        hdfs datanode &
        yarn resourcemanager &
        yarn nodemanager &
    fi
fi

sleep infinity
