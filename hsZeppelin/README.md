# hsZeppelin

Docker image containing Apache Hadoop, Apache Spark and Apache Zeppelin. Like
*hsBase* it is preconfigured for using hdfs and Yarn in pseudo-distributed mode.

## Build

To build the image run the command `docker build -t <tag> .` in the `hsZeppelin`
folder of the project. Replace `<tag>` with a tag of your choosing to easily
reference the image later for builds. The suggested tag is `danlilja/hszeppelin`,
which will be used for the remainder of this document.

## Usage

To start Zeppelin notebook simply use the command `docker run -it -p 8080:8080
danlilja/hszeppelin` and enter the webui at `localhost:8080`. If you want to run
a different command use `docker run -it --entrypoint <command>
danlilja/hszeppelin` where `<command>` is the command you wish to run instead of
starting Zeppelin, e.g. `bash`.

The image accepts two command line arguments, namely `--hadoop` and `--format`.
The option `--hadoop` also starts hdfs and Yarn in addition to Zeppelin. The
option `--format` formats the hdfs namenode and has no effect unless used
together with `--hadoop`. For example, use `docker run -it -P
danlilja/hszeppelin --hadoop --format` to publish all exposed ports to random
ports on the host, format the hdfs namenode, start hdfs, start Yarn and finally
start Zeppelin.

## Ports

The following ports are exposed by the dockerfile:

- Zeppelin webUI: 8080
- hdfs webUI: 50070
- Yarn webUI: 8088
- Spark master webUI: 7070
- Spark worker webUI: 8081

Note that the Spark master webUI port is not the default 8080 to avoid collision with
Zeppelin which also uses port 8080 by default.
