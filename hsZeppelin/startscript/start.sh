#! /bin/bash

hadoop=0
format=0

while [[ $1 != "" ]]; do
    case $1 in
        --hadoop )
            hadoop=1
            ;;
        --format )
            format=1
            ;;
    esac
    shift
done

if [[ $hadoop = 1 ]]; then
    /etc/init.d/ssh start
    if [[ $format = 1 ]]; then
        hdfs namenode -format
    fi
    hdfs namenode &
    hdfs datanode &
    yarn resourcemanager &
    yarn nodemanager &
fi

zeppelin.sh
