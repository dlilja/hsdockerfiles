# hsJupyter

Docker image containing Apache Hadoop, Apache Spark and Jupyter with Toree
kernels for Spark support in Jupyter notebooks. Like *hsBase* it is
preconfigured for using hdfs and Yarn in pseudo-distributed mode.

## Build

To build the image run the command `docker build -t <tag> .` in the `hsJupyter`
folder of the project. Replace `<tag>` with a tag of your choosing to easily
reference the image later for builds. The suggested tag is `danlilja/hsjupyter`,
which will be used for the remainder of this document.

## Usage

To start Jupyter notebook simply use the command `docker run -it -p 8888:8888
danlilja/hsjupyter` and use the token printed to stdout to enter the webui at
`localhost:8888`. If you want to run a different command use `docker run -it
--entrypoint <command> danlilja/hsjupyter` where `<command>` is the command you
wish to run instead of starting Jupyter, e.g. `bash`.

The image accepts three command line arguments, namely `--hadoop`, `--format`
and `--password`. The option `--hadoop` also starts hdfs and Yarn in addition to
Jupyter. The option `--format` formats the hdfs namenode and has no effect
unless used together with `--hadoop`. The option `--password` requires an
additional string and will set the Jupyter notebook password to the supplied
string. This allows you to log in to the Jupyter notebook server using that
password instead of a token. For example, use `docker run -it -P
danlilja/hsjupyter --hadoop --format --password hello` to publish all exposed
ports to random ports on the host, format the hdfs namenode, start hdfs, start
Yarn, set the Jupyter notebook server password to `hello` and finally start
Jupyter.

The Toree Scala and SQL interpreters are preinstalled for using Spark with Scala
and SQL. For using PySpark the package `findspark` is also installed. To use
PySpark and have a Spark context available as `sc`, start a Python3 notebook and
run

```python
import findspark
findspark.init()
import pyspark
sc = pyspark.SparkContext()
```

## Ports

The following ports are exposed by the dockerfile:

- Jupyter webUI: 8888
- hdfs webUI: 50070
- Yarn webUI: 8088
- Spark master webUI: 7070
- Spark worker webUI: 8081

Note that the Spark master webUI port is not the default 8080 to avoid collision with
Zeppelin which also uses port 8080 by default.
