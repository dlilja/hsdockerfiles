#! /bin/bash

hadoop=0
format=0
setpass=0
password=""
hashedPassword=""

while [[ $1 != "" ]]; do
    case $1 in
        --hadoop )
            hadoop=1
            ;;
        --format )
            format=1
            ;;
        --password )
            setpass=1
            shift
            password=$1
            ;;
    esac
    shift
done

if [[ $hadoop = 1 ]]; then
    /etc/init.d/ssh start
    if [[ $format = 1 ]]; then
        hdfs namenode -format
    fi
    hdfs namenode &
    hdfs datanode &
    yarn resourcemanager &
    yarn nodemanager &
fi
if [[ $setpass = 1 ]]; then
    hashedPassword=$(python3 -c "from notebook.auth import passwd; print(passwd(\"$password\"))")
    jupyter notebook --generate-config
    echo "c.NotebookApp.password = '$hashedPassword'" > /root/.jupyter/jupyter_notebook_config.py
fi

jupyter notebook --no-browser --allow-root --ip=0.0.0.0
