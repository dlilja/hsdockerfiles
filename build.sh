#! /bin/bash

if [[ $1 = "--tag" ]]; then
    docker build -t danlilja/hsbase:$2 ./hsBase/
    docker build -t danlilja/hszeppelin:$2 ./hsZeppelin/
    docker build -t danlilja/hsjupyter:$2 ./hsJupyter/
    docker build -t danlilja/hskafka:$2 ./hsKafka/
else
    docker build -t danlilja/hsbase ./hsBase/
    docker build -t danlilja/hszeppelin ./hsZeppelin/
    docker build -t danlilja/hsjupyter ./hsJupyter/
    docker build -t danlilja/hskafka ./hsKafka/
fi
